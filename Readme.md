# Software Studio 2021 Spring Midterm Project


## Topic
* Project Name : Online Chat Room

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|15%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|20%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

# 作品網址：https://chatroom-de0a3.web.app/

## Website Detail Description
我的Online Chat room是以sign in畫面的css Animation為主進行顏色和底圖的搭配。而版面配置的部分就是參考主流聊天室軟體的排版形式進行設計：左邊為Chatroom list、右邊為Message List。


# Components Description : 
1. Create button + input : 以input的值為聊天室名稱，創立新聊天室，若聊天室名稱重複會跳出alert。
2. Chatroom List : 聊天室列表，列出所有成員有你在內的聊天室。
3. Now in Chatroom [chatroom name] : 目前在某聊天室的畫面
4. Invite button + input : 以input的值為邀請成員名稱來邀請成員，會跳出notification顯示誰進入了聊天室。
5. Member button : 查看目前聊天室成員，並且可透過名稱旁的kick按鈕來剔除成員。
6. Chat button : 回到聊天室主頁面以顯示訊息。

# Other Functions Description : 
1. Notification : 在新成員進入聊天室 & 當前聊天室有新訊息時跳出。
2. Message : 可以顯示html code。
3. webpage icon : 在分頁標籤會顯示可愛的小蠟燭icon。

