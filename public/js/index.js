var user_email;
var str_before_username = "<p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><div class='chat'>";
var str_after_content = "<div></p>";

var string_before_button = "<button type='button' class='btn btn-lg btn-block roombtn' style='height:70px; font-size:30px' onclick='chooseroom(&quot;";
var string_in_button = "&quot;)'>";
var string_after_button = "</button>\n";

function init() {
    user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;
            dropdownMenuButton.innerHTML = user_email;
            menu.innerHTML = "<span class='log' id='logout-btn'>Logout</span>";
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            var logoutbtn = document.getElementById('logout-btn');
            logoutbtn.addEventListener('click', function () {
                firebase.auth().signOut().then(function () {
                    // Signed in
                    window.location.replace("/index.html");
                }).catch(function (error) {
                    txtEmail.value = "";
                    txtPassword.value = "";
                    create_alert("error", error.message);
                });
            });

        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='log' href='signin.html'>Login</a>";
            document.getElementById('chat_list').innerHTML = "";
            document.getElementById('post_list').innerHTML = "";
        }
    });

    //create
    var create_btn = document.getElementById('create_btn');
    var chat = document.getElementById('createinput');


    create_btn.addEventListener('click', function () {
        if (chat.value != "") {
            var Ref = firebase.database().ref("room_list");
            var chatname = document.getElementById('createinput').value;
            var flag = 0;
            roomsRef.once('value', function (snapshot) {
                snapshot.forEach(function (childshot) {
                    var childData = childshot.val();
                    if (childData.roomname === chatname) {
                        alert("This Chatroom exists!");
                        flag = 1;
                    }
            })
            }).then(function(snapshot) {
                if(flag===0)
                {   
                    alert('Create New Room Success!');
                    var data = {
                        roomname:chatname
                    };
                    Ref.push(data);
                    chat.value = "";
                    var Ref2 = firebase.database().ref(chatname+"/member");
                    data = {
                        member:user_email
                    };
                    Ref2.push(data);

                    var today = new Date();
                    var currentDateTime =
                    today.getFullYear() + '/' +
                    (today.getMonth() + 1) + '/' +
                    today.getDate() + " " + today.getHours() + ':' + today.getMinutes();

                    var Ref3 = firebase.database().ref(chatname+"/com_list");
                    data = {
                        time: currentDateTime,
                        roomname: chatname,
                        sender: "Welcome to chatroom "+chatname,
                        data: ""
                    };
                    Ref3.push(data);
                    document.getElementById('nameinput').innerHTML = chatname;
                    notifyMe(user_email+" has entered the room");

                    var postsRef = firebase.database().ref(document.getElementById('nameinput').innerHTML+"/com_list");
                    // List for store posts html
                    var total_post = [];
                    // Counter for checking history post update complete
                    var first_count = 0;
                    // Counter for checking when to update new post
                    var second_count = 0;
                    
                    postsRef.once('value')
                        .then(function (snapshot) {
                            /// TODO 7: Get all history posts when the web page is loaded 
                            ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
                            ///         2. count history message number and recond in "first_count"
                            ///         Hint : Trace the code in this block, then you will know how to finish this TODO
                            snapshot.forEach(function (childshot) {
                                var childData = childshot.val();
                                if(childData.sender == user_email)
                                    {
                                        total_post[total_post.length] = "<div class='a'><div class='b'>"+childData.time+
                                        "<strong class='d-block text-gray-dark' style='font-size:20px'>"+childData.sender+
                                        "</strong><strong></strong><br><strong class='d-block text-gray-dark' style='font-size:30px'>"+childData.data+"</strong></div></div>";    
                                    }
                                    else
                                    {
                                        total_post[total_post.length] = "<div class='a'><div class='c'>"+childData.time+
                                        "<strong class='d-block text-gray-dark' style='font-size:20px'>"+childData.sender+
                                        "</strong><strong></strong><br><strong class='d-block text-gray-dark' style='font-size:30px'>"+childData.data+"</strong></div></div>";    
                                    }
                                first_count += 1;
                            });
                            /// Join all post in list to html in once
                            document.getElementById('post_list').innerHTML = total_post.join('');

                            // postsRef.on('child_added', function (data) {
                            //     second_count += 1;
                            //     if (second_count > first_count) {
                            //         var childData = data.val();
                            //         if(childData.sender == user_email)
                            //         {
                            //             total_post[total_post.length] = "<div class='a'><div class='b'>"+childData.time+
                            //             "<strong class='d-block text-gray-dark' style='font-size:20px'>"+childData.sender+
                            //             "</strong><strong></strong><br><strong class='d-block text-gray-dark' style='font-size:30px'>"+childData.data+"</strong></div></div>";    
                            //         }
                            //         else
                            //         {
                            //             total_post[total_post.length] = "<div class='a'><div class='c'>"+childData.time+
                            //             "<strong class='d-block text-gray-dark' style='font-size:20px'>"+childData.sender+
                            //             "</strong><strong></strong><br><strong class='d-block text-gray-dark' style='font-size:30px'>"+childData.data+"</strong></div></div>";    
                            //         }
                            //     document.getElementById('post_list').innerHTML = total_post.join('');
                            //     }
                            // });
                        })
                        .catch(e => console.log(e.message));

                        var roomsRef = firebase.database().ref('room_list');
                        // List for store posts html
                        var room_post = [];
                        // Counter for checking history post update complete
                        var room_first_count = 0;
                        // Counter for checking when to update new post
                        var room_second_count = 0;

                        var flag2 = 0;
                        
                        document.getElementById('chat_list').innerHTML="";

                        roomsRef.once('value')
                            .then(function (snapshot) {
                                snapshot.forEach(function (childshot) {
                                    var childData = childshot.val();
                                    var rn = childData.roomname;
                                    
                                    var flag1 = 0;
                                    var rref = firebase.database().ref(rn+"/member");
                                    rref.once('value')
                                    .then(function (ss) {
                                        ss.forEach(function(cs){
                                            var c = cs.val();
                                            if(c.member===user_email)
                                            {
                                                flag1=1;
                                            }
                                        });
                                        if(flag1==1)
                                        {
                                            console.log("2"+childData.roomname);
                                            room_post[room_post.length] = string_before_button + childData.roomname + string_in_button + childData.roomname + string_after_button;
                                            room_first_count += 1;
                                            flag2 = 1;
                                        }
                                        document.getElementById('chat_list').innerHTML = room_post.join('');
                                    })
                                    .catch(e => console.log(e.message));
                                });
                                document.getElementById('chat_list').innerHTML = room_post.join('');

                                // roomsRef.on('child_added', function (data) {
                                //     room_second_count += 1;
                                //     if (room_second_count > room_first_count) {
                                //         var childData = data.val();
                                //         if(flag2 == 1)
                                //         {
                                //             console.log("3"+childData.roomname);
                                //             room_post[room_post.length] = string_before_button + childData.roomname + string_in_button + childData.roomname + string_after_button;
                                //         }
                                //         document.getElementById('chat_list').innerHTML = room_post.join('');
                                //     }
                                // });
                            })
                            .catch(e => console.log(e.message));

                };
            }).catch(e => console.log(e.message));
        }
    });


//create
var invite_btn = document.getElementById('invite_btn');
var invite_name = document.getElementById('invitename');

invite_btn.addEventListener('click', function () {
    if (invite_name.value != "") {

        var chatroom = document.getElementById('nameinput').innerHTML;
        var Ref = firebase.database().ref(chatroom + "/member");
        if (chatroom != "") {
            data = {
                member: invite_name.value
            };
            notifyMe(invite_name.value+" has entered the room");
            invite_name.value = "";
            Ref.push(data);
            
        }
        else {
            alert("Please Enter a room!");
        }

    }
});

//post
post_btn = document.getElementById('post_btn');
post_txt = document.getElementById('comment');

post_btn.addEventListener('click', function () {
    if (post_txt.value != "") {
        /// TODO 6: Push the post to database's "com_list" node
        ///         1. Get the reference of "com_list"
        ///         2. Push user email and post data
        ///         3. Clear text field
        var today = new Date();
        var currentDateTime =
            today.getFullYear() + '/' +
            (today.getMonth() + 1) + '/' +
            today.getDate() + " " + today.getHours() + ':' + today.getMinutes();
        var name = document.getElementById('nameinput').innerHTML;
        var Ref = firebase.database().ref(name+"/com_list");
        var s = html_encode(post_txt.value);
        var data = {
            time: currentDateTime,
            roomname: name,
            sender: user_email,
            data: s
        };
        Ref.push(data);
        post_txt.value = "";

    }
});


var roomsRef = firebase.database().ref('room_list');
// List for store posts html
var room_post = [];
// Counter for checking history post update complete
var room_first_count = 0;
// Counter for checking when to update new post
var room_second_count = 0;

var flag1 = 0;
document.getElementById('chat_list').innerHTML="";
roomsRef.once('value')
    .then(function (snapshot) {
        snapshot.forEach(function (childshot) {
            var childData = childshot.val();
            var rn = childData.roomname;
            
            var flag1 = 0;
            var rref = firebase.database().ref(rn+"/member");
            rref.once('value')
            .then(function (ss) {
                ss.forEach(function(cs){
                    var c = cs.val();
                    if(c.member===user_email)
                    {
                        flag1=1;
                    }
                });
                if(flag1==1)
                {
                    room_post[room_post.length] = string_before_button + childData.roomname + string_in_button + childData.roomname + string_after_button;
                    room_first_count += 1;
                }
                document.getElementById('chat_list').innerHTML = room_post.join('');
            })
            .catch(e => console.log(e.message));
        });
        document.getElementById('chat_list').innerHTML = room_post.join('');

        // roomsRef.on('child_added', function(data) {
        //     room_second_count += 1;
        //     if (room_second_count > room_first_count) {
        //         var childData = data.val();
        //         var rn = childData.roomname;
            
        //         var flag1 = 0;
        //         var rref = firebase.database().ref(rn+"/member");
        //         rref.once('value')
        //         .then(function (ss) {
        //             ss.forEach(function(cs){
        //                 var c = cs.val();
        //                 if(c.member===user_email)
        //                 {
        //                     flag1=1;
        //                 }
        //             });
        //             if(flag1==1)
        //             {
        //                 room_post[room_post.length] = string_before_button + childData.roomname + string_in_button + childData.roomname + string_after_button;
        //             }
        //             document.getElementById('chat_list').innerHTML = room_post.join('');
        //         })
        //         .catch(e => console.log(e.message));
                
        //         document.getElementById('chat_list').innerHTML = room_post.join('');
        //     }
        // });

    })
    .catch(e => console.log(e.message));

// var postsRef = firebase.database().ref(document.getElementById('nameinput').innerHTML+"/com_list");
// // List for store posts html
// var total_post = [];
// // Counter for checking history post update complete
// var first_count = 0;
// // Counter for checking when to update new post
// var second_count = 0;

// postsRef.once('value')
//     .then(function (snapshot) {
//         /// TODO 7: Get all history posts when the web page is loaded 
//         ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
//         ///         2. count history message number and recond in "first_count"
//         ///         Hint : Trace the code in this block, then you will know how to finish this TODO
//         snapshot.forEach(function (childshot) {
//             var childData = childshot.val();
//             total_post[total_post.length] = childData.time + str_before_username + childData.sender + childData.data + "</strong>"+ str_after_content;
//             first_count += 1;
//         });
//         /// Join all post in list to html in once
//         document.getElementById('post_list').innerHTML = total_post.join('');

//         postsRef.on('child_added', function (data) {
//             second_count += 1;
//             if (second_count > first_count) {
//                 var childData = data.val();
//                 console.log("2");
//                 total_post[total_post.length] = childData.time + str_before_username + childData.sender + childData.data + "</strong>"+ str_after_content;
//                 document.getElementById('post_list').innerHTML = total_post.join('');
//             }
//         });
//     })
//     .catch(e => console.log(e.message));
}

window.onload = function () {
    init();
};

// roomsRef.once('value')
//     .then(function (snapshot) {
//         snapshot.forEach(function (childshot) {
//             var childData = childshot.val();
//             room_post[room_post.length] = string_before_button + childData.roomname + string_in_button +childData.roomname + string_after_button;
//             room_first_count += 1;
//         });
//         document.getElementById('chat_list').innerHTML = room_post.join('');

//         roomsRef.on('child_added', function (data) {
//             room_second_count += 1;
//             if (room_second_count > room_first_count) {
//                 var childData = data.val();
//                 room_post[room_post.length] = string_before_button + childData.roomname + string_in_button + childData.roomname + string_after_button;
//                 document.getElementById('chat_list').innerHTML = room_post.join('');
//             }
//         });
//     })
//     .catch(e => console.log(e.message));

function chooseroom(v){
    document.getElementById('nameinput').innerHTML = v;
    var postsRef = firebase.database().ref(v+'/com_list');
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    postsRef.once('value')
        .then(function(snapshot) {
            /// TODO 7: Get all history posts when the web page is loaded 
            ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
            ///         2. count history message number and recond in "first_count"
            ///         Hint : Trace the code in this block, then you will know how to finish this TODO
            snapshot.forEach(function(childshot) {
                var childData = childshot.val();
                if(childData.sender == user_email)
                {
                    total_post[total_post.length] = "<div class='a'><div class='b'>"+childData.time+
                    "<strong class='d-block text-gray-dark' style='font-size:20px'>"+childData.sender+
                    "</strong><strong></strong><br><strong class='d-block text-gray-dark' style='font-size:25px'>"+childData.data+"</strong></div></div>";    
                }
                else
                {
                    total_post[total_post.length] = "<div class='a'><div class='c'>"+childData.time+
                    "<strong class='d-block text-gray-dark' style='font-size:20px'>"+childData.sender+
                    "</strong><strong></strong><br><strong class='d-block text-gray-dark' style='font-size:25px'>"+childData.data+"</strong></div></div>";    
                }
                first_count += 1;
            });
            /// Join all post in list to html in once
            document.getElementById('post_list').innerHTML = total_post.join('');

            /// Add listener to update new post
            postsRef.on('child_added', function(data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    notifyMe(childData.sender+" : "+childData.data);
                    if(childData.sender == user_email)
                    {
                        total_post[total_post.length] = "<div class='a'><div class='b'>"+childData.time+
                        "<strong class='d-block text-gray-dark' style='font-size:20px'>"+childData.sender+
                        "</strong><strong></strong><br><strong class='d-block text-gray-dark' style='font-size:25px'>"+childData.data+"</strong></div></div>";    
                    }
                    else
                    {
                        total_post[total_post.length] = "<div class='a'><div class='c'>"+childData.time+
                        "<strong class='d-block text-gray-dark' style='font-size:20px'>"+childData.sender+
                        "</strong><strong></strong><br><strong class='d-block text-gray-dark' style='font-size:25px'>"+childData.data+"</strong></div></div>"; 
                          
                    }
                    document.getElementById('post_list').innerHTML = total_post.join('');
                }
            });
        })
        .catch(e => console.log(e.message));
}


function member()
{
    var v=document.getElementById('nameinput').innerHTML;

    var postsRef = firebase.database().ref(v+'/member');
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    postsRef.once('value')
        .then(function(snapshot) {
                snapshot.forEach(function(childshot) {
                var childData = childshot.val();
                    total_post[total_post.length] = "<div class='d'>"+
                    "<strong class='d-block text-gray-dark' style='font-size:20px'>"+childData.member+
                    "<button type='button' class='btn btn-primary' style='margin-left:10px;height:30px' onclick='kick(&quot;"+childData.member+
                    "&quot;)'> kick </button></div>";
                first_count += 1;
            });
            /// Join all post in list to html in once
            document.getElementById('post_list').innerHTML = total_post.join('');

            /// Add listener to update new post
            postsRef.on('child_added', function(data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();

                        total_post[total_post.length] = "<div class='d'>"+
                        "<strong class='d-block text-gray-dark' style='font-size:20px'>"+childData.member+
                        "<button type='button' class='btn btn-primary' style='margin-left:10px;height:30px' onclick='kick(&quot;"+childData.member+
                        "&quot;)'> kick </button></div>";


                    document.getElementById('post_list').innerHTML = total_post.join('');
                }
            });
        })
        .catch(e => console.log(e.message));
}



function kick(v)
{
    a = document.getElementById('nameinput').innerHTML;
    
    var postsRef = firebase.database().ref(a+'/member');
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;
    postsRef.once('value')
        .then(function(snapshot) {
            snapshot.forEach(function(childshot) {
                var childData = childshot.val();
                var key = childshot.key;
                if(childData.member != v)
                {
                    total_post[total_post.length] = "<div class='d'>"+
                    "<strong class='d-block text-gray-dark' style='font-size:20px'>"+childData.member+
                    "<button type='button' class='btn btn-primary' style='margin-left:10px;height:30px' onclick='kick(&quot;"+childData.member+
                    "&quot;)'> kick </button></div>";
                    first_count += 1;
                }
                else
                {
                    console.log(childData.member);
                    var f=0;
                    if(childData.member == user_email)
                        {
                            document.getElementById('nameinput').innerHTML="";
                            document.getElementById('post_list').innerHTML="";
                            f=1;
                        }
                    firebase.database().ref(a+'/member/'+key).remove();
                    if(f==1)
                    {
                        throw {};
                    }
                }
            });
            document.getElementById('post_list').innerHTML = total_post.join('');
            /// Add listener to update new post

            // postsRef.on('child_added', function(data) {
            //     second_count += 1;
            //     if (second_count > first_count) {
            //         var childData = data.val();
            //         if(childData.member != v)
            //         {
            //             total_post[total_post.length] = "<div class='d'>"+
            //             "<strong class='d-block text-gray-dark' style='font-size:20px'>"+childData.member+
            //             "<button type='button' class='btn btn-primary' style='margin-left:10px;height:30px' onclick='kick(&quot;"+childData.member+
            //             "&quot;)'> kick </button></div>";
            //             first_count += 1;
            //         }
            //         else
            //         {
            //             console.log(childData.member);
            //             console.log("c"+user_email);
            //             if(childData.member == user_email)
            //             {
            //                 document.getElementById('nameinput').innerHTML="";
            //                 document.getElementById('post_list').innerHTML="";
            //             }
            //             firebase.database().ref(a+'/member/'+key).remove();
            //         }
            //         document.getElementById('post_list').innerHTML = total_post.join('');
            //     }
            // });
        })
        .catch(e => console.log(e.message));

        var roomsRef = firebase.database().ref('room_list');
        // List for store posts html
        var room_post = [];
        // Counter for checking history post update complete
        var room_first_count = 0;
        // Counter for checking when to update new post
        var room_second_count = 0;
        
        var flag1 = 0;
        document.getElementById('chat_list').innerHTML="";
        roomsRef.once('value')
            .then(function (snapshot) {
                snapshot.forEach(function (childshot) {
                    var childData = childshot.val();
                    var rn = childData.roomname;
                    
                    var flag1 = 0;
                    var rref = firebase.database().ref(rn+"/member");
                    rref.once('value')
                    .then(function (ss) {
                        ss.forEach(function(cs){
                            var c = cs.val();
                            if(c.member===user_email)
                            {
                                flag1=1;
                            }
                        });
                        if(flag1==1)
                        {
                            console.log("1"+childData.roomname);
                            room_post[room_post.length] = string_before_button + childData.roomname + string_in_button + childData.roomname + string_after_button;
                            room_first_count += 1;
                        }
                        document.getElementById('chat_list').innerHTML = room_post.join('');
                    })
                    .catch(e => console.log(e.message));
                });
                document.getElementById('chat_list').innerHTML = room_post.join('');
        
            })
            .catch(e => console.log(e.message));
}



function chatbtn()
{
    v = document.getElementById('nameinput').innerHTML;
    var postsRef = firebase.database().ref(v+'/com_list');
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    postsRef.once('value')
        .then(function(snapshot) {
            /// TODO 7: Get all history posts when the web page is loaded 
            ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
            ///         2. count history message number and recond in "first_count"
            ///         Hint : Trace the code in this block, then you will know how to finish this TODO
            snapshot.forEach(function(childshot) {
                var childData = childshot.val();
                if(childData.sender == user_email)
                {
                    total_post[total_post.length] = "<div class='a'><div class='b'>"+childData.time+
                    "<strong class='d-block text-gray-dark' style='font-size:20px'>"+childData.sender+
                    "</strong><strong></strong><br><strong class='d-block text-gray-dark' style='font-size:25px'>"+childData.data+"</strong></div></div>";    
                }
                else
                {
                    total_post[total_post.length] = "<div class='a'><div class='c'>"+childData.time+
                    "<strong class='d-block text-gray-dark' style='font-size:20px'>"+childData.sender+
                    "</strong><strong></strong><br><strong class='d-block text-gray-dark' style='font-size:25px'>"+childData.data+"</strong></div></div>";    
                }
                first_count += 1;
            });
            /// Join all post in list to html in once
            document.getElementById('post_list').innerHTML = total_post.join('');

            /// Add listener to update new post
            postsRef.on('child_added', function(data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    if(childData.sender == user_email)
                    {
                        total_post[total_post.length] = "<div class='a'><div class='b'>"+childData.time+
                        "<strong class='d-block text-gray-dark' style='font-size:20px'>"+childData.sender+
                        "</strong><strong></strong><br><strong class='d-block text-gray-dark' style='font-size:25px'>"+childData.data+"</strong></div></div>";    
                    }
                    else
                    {
                        total_post[total_post.length] = "<div class='a'><div class='c'>"+childData.time+
                        "<strong class='d-block text-gray-dark' style='font-size:20px'>"+childData.sender+
                        "</strong><strong></strong><br><strong class='d-block text-gray-dark' style='font-size:25px'>"+childData.data+"</strong></div></div>";    
                    }
                    document.getElementById('post_list').innerHTML = total_post.join('');
                }
            });
        })
        .catch(e => console.log(e.message));
}

function html_encode(html)
{
　　return document.createElement('div')
　　.appendChild(document.createTextNode(html))
　　.parentNode.innerHTML;
}

function notifyMe(v) {
    // 首先讓我們確定瀏覽器支援 Notification
    if (!("Notification" in window)) {
      alert("這個瀏覽器不支援 Notification");
    }
  
    // 再檢查使用者是否已經授權執行 Notification
    else if (Notification.permission === "granted") {
      // 如果已經授權就可以直接新增 Notification 了!
      var notification = new Notification(v);
    }
  
    // 否則，我們會需要詢問使用者是否開放權限
    else if (Notification.permission !== 'denied') {
      Notification.requestPermission(function (permission) {
        // 如果使用者同意了就來新增一個 Notification 打聲招呼吧
        if (permission === "granted") {
          var notification = new Notification("權限開啟!");
        }
      });
    }
  }